module ApplicationHelper
  def page_title(title, prefix_with_app_name=true)
    # content_for(:title) do
    #    prefix_with_app_name ?  ENV['APP_NAME'] + " - " + title : title
    # end
    "dd"
  end

  def page_header(text)
    content_tag 'h1' , text , class: 'content-title'
  end

  def nav_item(title, url, current_item, options={})
    content_tag 'li', class: ('active' if params[:action] == current_item) do
      link_to title, url, options
    end
  end

  def flash_messages
    trans = { 'alert' => 'alert-danger', 'notice' => 'alert alert-success' }
    flash.map do |key, value|
      content_tag 'div', value, class: "alert #{trans[key]} pt_fade"
    end.join('.').html_safe
  end

  def timeago_tag(datetime)
    content_tag :span, time_ago_in_words(datetime),
                       class: 'time-ago p-date',
                       title: datetime
  end

  def social_share_link(type, options, &block)
    link_to '', class: "share s_#{type}", title: type, data: options, &block
  end

  def social_share_facebook(options, &block)
    social_share_link 'facebook', options, &block
  end

  def social_share_twitter(options, &block)
    social_share_link 'twitter', options, &block
  end

  def social_share_tumbler(options, &block)
    social_share_link 'tumblr', options, &block
  end

  def social_share_email(options, &block)
    subject = "Check out this post on #{ENV['APP_NAME']}"
    body    = "#{options[:description]} #{options[:share_url]}"
    mail_to "", class:'share', subject: subject , body: body, &block
  end

  def social_share_options(post_page)
    { title:       "#{post_page.dog_name}'s post",
      description: post_page.description,
      image:       image_url(post_page.photo.url),
      share_url:   request.original_url
    }
  end
end
