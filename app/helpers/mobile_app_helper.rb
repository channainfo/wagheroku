module MobileAppHelper
  def mobile_launcer_reset_password_params(user)
    path = edit_password_path(
      user_id: user.id,
      reset_password_token: user.reset_password_token
    )

    mobile_app_params(path)
  end

  def mobile_launcer_post_params(post)
    path = post_path(post.id)
    mobile_app_params(path)
  end

  def mobile_app_params(path)
    {
      app_store_url: ENV["APP_STORE_URL"],
      app_url_path:  ENV['APP_SCHEME'] + path.from(1)
    }
  end
end
