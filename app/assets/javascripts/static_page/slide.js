$(function(){
  initFullPage();
});

function initFullPage(){
  var pageDesktop = $("#page-desktop").length > 0 ;
  var pageMobile  = $("#page-mobile").length > 0 ;

  if(pageDesktop){
    $('#page-desktop').fullpage(optionsDesktop());
  }
  else if (pageMobile) {
    $('#page-mobile').fullpage(optionsMobile());
  }
}

function optionsDesktop(){
  var options = {
    verticalCentered: true,
    anchors: ['page1', 'page2', 'page3', 'page4'],
    css3: true

    // scrollingSpeed: 200,
    // navigation: true,
    // navigationPosition: 'right'
  }
  return options;
}

function optionsMobile(){
  var options = {
    verticalCentered: true,
    anchors: ['page1', 'page2', 'page3', 'page4'],
    css3: true
    // scrollingSpeed: 200,
    // navigation: false
  }

  return options;
}

