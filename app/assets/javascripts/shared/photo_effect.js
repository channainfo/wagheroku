$(function(){
  photoEffect();
})

function photoEffect() {

  var duration = 700;
  $photoEffect = $(".photo-effect");


  $photoEffect
   .mouseenter(function(e){
     $(this).animate({ opacity: 0 }, duration);
   })
   .mouseleave(function(e){
     $(this).animate({ opacity: 0.3 }, duration);
  })
}