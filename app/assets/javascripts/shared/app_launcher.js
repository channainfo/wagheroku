$(function(){
  initAppLauncher();
});

function initAppLauncher(){
  $(".pt-app-launcher").on('click', function(){
    config = $(this).data();

    setTimeout(function () {
        openAppUrl(config.appStoreUrl);
    }, 20);
    openAppUrl(config.appUrlPath);

    return false;
  })
}

function openAppUrl(path){
  window.location.href = path;
}
