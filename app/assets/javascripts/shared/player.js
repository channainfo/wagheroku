$(function(){
  responsiveVideo();
})

function responsiveVideo() {
  sublime.ready(function(){
    $(".video").each(function() {
      var element = this
      sublime.prepare(element, function(player){
        resizeVideo(element);
        $(".video").css('visibility', 'visible');
      })
    });


  })

  $(window).resize(function() {
    resizeVideos();
  });
}

function resizeVideo(element){
  if(sublime) {
    var player = sublime.player(element);
    if(player) {
      $parent = $(element).closest(".video-c");
      var width = $parent.width();
      var height = width;
      player.setSize(width, height);
    }
  }
}

function resizeVideos(){
  $(".video").each(function() {
    resizeVideo(this);
  });
}