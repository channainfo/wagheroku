//= require jquery
//= require jquery_ujs

//= require bootstrap

//= require rails-timeago

//= require post_page

//= require shared/share
//= require shared/app_launcher
//= require shared/player
//= require shared/time_ago
//= require shared/photo_effect
