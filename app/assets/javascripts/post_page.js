$(function(){
  popularPostsResize();
})

$(window).resize(function() {
  popularPostsResize();
})

function popularPostsResize(){
  var $populars = $(".popular-responsive-c");
  $populars.each(function(i){
    var $popular = $($populars[i]);
    var width = $popular.width();

    var padding = 20 ;
    var margin  = 6;

    var availableSpace =  width - padding - 2*margin;

    var width = availableSpace/3;
    var $image = $popular.find(".popular-item");

    $image.css('width', width + "px");
    $image.css('height', width + "px");
  })
}