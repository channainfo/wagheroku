class ApplicationController < ActionController::Base
  protect_from_forgery

  before_action :set_device_type
  helper_method :on_iphone?, :on_browser_chrome?, :on_mobile?, :on_mobile_and_tablet?

  protected

  def set_device_type
    request.variant = :iphone if on_iphone?
  end

  def on_iphone?
    request.user_agent =~ /iPhone/i
  end

  def on_browser_chrome?
    request.user_agent =~ /CriOS/i
  end

  def on_mobile?
    request.user_agent =~ /Mobile|webOS/i
  end

  def on_mobile_and_tablet?
    request.env["HTTP_USER_AGENT"] && request.env["HTTP_USER_AGENT"][/(iPhone|iPad|iPod|BlackBerry|Android|Mobile|webOS)/i]
  end
end