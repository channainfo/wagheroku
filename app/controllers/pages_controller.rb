class PagesController < ApplicationController
  layout 'static_page'

  def home
    # # TODO: Remove it. It's used for testing purpose
    # $pubnub.publish(channel: 'welcome', message: 'Hello Petto!') do |envelope|
    #   logger.info "Publish welcome message"
    # end
    request.variant = :mobile if on_mobile?
  end

  def landing
    request.variant = :mobile if on_mobile?
  end
end
